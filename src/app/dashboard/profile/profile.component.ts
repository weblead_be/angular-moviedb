import { AuthService } from './../../shared/authentication/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ProfileService } from './profile.service';
import { AlertService } from 'src/app/shared/alert/alert.service';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  
  profileForm: FormGroup;
  userDetails;
  userProfile;

  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;

  constructor( 
    private formBuilder: FormBuilder, 
    private authService: AuthService, 
    public profileService: ProfileService, 
    private alertService: AlertService,
    private storage: AngularFireStorage
  ) {
    
    this.authService.user.subscribe(
      (user) => {
        if (user) {
          this.userDetails = user;

          this.getProfile(user.uid);
        }
        else {
          this.userDetails = null;
        }
      }
    );

  }

  ngOnInit() {

    this.profileForm = this.formBuilder.group({
      firstname: ['', [
        Validators.required
      ]],
      lastname: ['', [
        Validators.required
      ]],
      profile_picture: ['', [
        Validators.required
      ]]
    })

  }

  getProfile(userId:string) {
    this.profileService.getProfile(userId).subscribe(
      (response) => {
        this.userProfile = response;

        this.setFormData();

      }
    );
  }

  setFormData() {
    this.profileForm.setValue({
      firstname: this.userProfile.firstname,
      lastname: this.userProfile.lastname,
      profile_picture: this.userProfile.profile_picture
    });
  }

  profileSubmitHandler() {
    const formValue = this.profileForm.value;

    this.profileService.updateProfile(formValue, this.userDetails.uid);

    this.alertService.success('Profile updated!');

  }

  uploadFile(event) {
    const file = event.target.files[0];
    const hash = Math.random().toString(36).substr(2, 5);
    const filePath = 'profile_pictures/'+ this.userDetails.uid + '_' + hash + '_' + file.name;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    // observe percentage changes
    this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(url => {
            this.profileForm.patchValue({
              profile_picture: url
            });
            this.userProfile.profile_picture = url;
          });
          
        })
     )
    .subscribe()
  }

}
