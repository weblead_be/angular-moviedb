import { Injectable } from '@angular/core';

import { AngularFireDatabase } from 'angularfire2/database';


@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor( private _firebaseDb: AngularFireDatabase ) { }

  getProfile(userId:string) {
    return this._firebaseDb.object('/users/'+userId).valueChanges();
  }

  updateProfile(formValue, userId) {
    this._firebaseDb.object('/users/'+userId).update({
      firstname: formValue.firstname,
      lastname: formValue.lastname,
      profile_picture: formValue.profile_picture
    });
  }
  

}
