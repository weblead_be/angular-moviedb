import { AuthService } from './../shared/authentication/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-root',
  templateUrl: './dashboard-root.component.html',
  styleUrls: ['./dashboard-root.component.scss']
})
export class DashboardRootComponent implements OnInit {
  
  loading: boolean = false;

  constructor( public authService: AuthService ) { }

  ngOnInit() {
  }

}
