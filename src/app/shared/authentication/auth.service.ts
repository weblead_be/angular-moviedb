import { AlertService } from './../alert/alert.service';
import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})

export class AuthService {
  user: Observable<firebase.User>;
  private userDetails: firebase.User = null;
  
  constructor(
    private _firebaseAuth: AngularFireAuth, 
    private router: Router,
    private alertService: AlertService
  ) { 

    this.user = _firebaseAuth.authState;
    

    this.user.subscribe(
      (user) => {
        if (user) {
          this.userDetails = user;
        }
        else {
          this.userDetails = null;
        }
      }
    );
  }

  signInRegular(email, password) {
    const credential = firebase.auth.EmailAuthProvider.credential( email, password );
    return this._firebaseAuth.auth.signInWithEmailAndPassword(email, password)
  }

  isLoggedIn() {
    if (this.userDetails == null ) {
      return false;
    } else {
      return true;
    }
  }

  logout() {
    this._firebaseAuth.auth.signOut()
    .then((res) => {
      this.router.navigate(['/'])
      this.alertService.success('You are signed out!');
    });
  }

}