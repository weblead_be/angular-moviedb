import { AuthService } from './../authentication/auth.service';
import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/dashboard/profile/profile.service';

@Component({
  selector: 'app-navbar-account',
  templateUrl: './navbar-account.component.html',
  styleUrls: ['./navbar-account.component.scss']
})
export class NavbarAccountComponent implements OnInit {

  menuOpen:boolean = false;
  userDetails;
  userProfile;

  constructor(public authService: AuthService, public profileService: ProfileService) {

    this.authService.user.subscribe(
      (user) => {
        if (user) {
          this.userDetails = user;

          this.getProfile(user.uid);
        }
        else {
          this.userDetails = null;
        }
      }
    );

  }

  ngOnInit() {    

  }

  getProfile(userId:string) {
    this.profileService.getProfile(userId).subscribe(
      (response) => {
        this.userProfile = response;
      }
    );
  }

}
