import { Reviews, ReviewsObject } from '../../movies/movie-detail/movie-reviews/movie-reviews.model';
import {first} from 'rxjs/operators';

import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute} from "@angular/router";
import { Observable } from 'rxjs';

import {Injectable} from "@angular/core";
import { ReviewsService } from 'src/app/movies/movie-detail/movie-reviews/reviews.service';



@Injectable()
export class MovieDetailReviewResolver implements Resolve<Reviews[]> {


    constructor(private reviewService: ReviewsService) {
    }

    resolve(route:ActivatedRouteSnapshot,
            state:RouterStateSnapshot):Observable<Reviews[]> {
                
        return this.reviewService.getReviewsList(route.params['id']).pipe(
            first());
    }

}