import {first} from 'rxjs/operators';

import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute} from "@angular/router";
import { MovieListRemote, MovieListRemoteObject } from '../../movies/movie-list/movie-list-remote.model';
import { Observable } from 'rxjs';

import {Injectable} from "@angular/core";
import {MovieService} from "../../movies/movie.service";



@Injectable()
export class MovieListResolver implements Resolve<MovieListRemoteObject> {


    constructor(private movieService: MovieService) {
    }

    resolve(route:ActivatedRouteSnapshot,
            state:RouterStateSnapshot):Observable<MovieListRemoteObject> {

        return this.movieService.getMoviesListRemote(route.params['id'], route.params['query'], route.params['page']).pipe(
            first());
    }

}