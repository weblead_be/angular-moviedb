import {first} from 'rxjs/operators';

import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute} from "@angular/router";
import { MovieListRemote } from '../../movies/movie-list/movie-list-remote.model';
import { Observable } from 'rxjs';

import {Injectable} from "@angular/core";
import {MovieService} from "../../movies/movie.service";
import { MovieDetailRemote } from '../../movies/movie-detail/movie-detail-remote.model';



@Injectable()
export class MovieDetailResolver implements Resolve<MovieDetailRemote> {


    constructor(private movieService: MovieService) {
    }

    resolve(route:ActivatedRouteSnapshot,
            state:RouterStateSnapshot):Observable<MovieDetailRemote> {
                
        return this.movieService.getMovieDetailRemote(route.params['id']).pipe(
            first());
    }

}