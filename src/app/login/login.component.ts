import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../shared/authentication/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  return: string = '';

  user = {
    email: '',
    password: ''
  };

  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    
    // Get the query params
    this.route.queryParams
      .subscribe(params => this.return = params['return'] || '/');

  }

  signInWithEmail() {
    this.authService.signInRegular(this.user.email, this.user.password)
        .then((res) => {
          this.router.navigateByUrl(this.return);
        })
        .catch((err) => console.log('error: ' + err));
  }

}
