import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MovieListComponent } from "./movies/movie-list/movie-list.component";
import { MovieDetailComponent } from "./movies/movie-detail/movie-detail.component";
import { MovieFavoritesComponent } from "./movies/movie-favorites/movie-favorites.component";
import { MovieListResolver } from "./shared/resolvers/movie-list.resolver";
import { MovieDetailResolver } from "./shared/resolvers/movie-detail.resolver";
import { MovieDetailReviewResolver } from "./shared/resolvers/movie-detail-review.resolver";
import { MovieRootComponent } from './movies/movie-root.component';
import { DashboardRootComponent } from './dashboard/dashboard-root.component';
import { LoginComponent } from './login/login.component';

import { AuthGuard } from './shared/authentication/auth-guard.service';
import { ProfileComponent } from './dashboard/profile/profile.component';

const routes: Routes = [
  
  {
    path: "",
    component: MovieRootComponent,
    children: [
      {
        path: "",
        component: MovieListComponent,
        resolve: { moviesRemote: MovieListResolver }
      },
      {
        path: "genre/:id",
        component: MovieListComponent,
        resolve: { moviesRemote: MovieListResolver }
      },
      { path: "search", redirectTo: "/", pathMatch: "full" },
      {
        path: "search/:query",
        component: MovieListComponent,
        resolve: { moviesRemote: MovieListResolver }
      },
      { path: "favorites", component: MovieFavoritesComponent },
      {
        path: "movie/:id",
        component: MovieDetailComponent,
        resolve: {
          movieRemote: MovieDetailResolver,
          reviews: MovieDetailReviewResolver
        }
      }
    ],
    
  },
  {
    path: "dashboard",
    component: DashboardRootComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "profile",
        component: ProfileComponent
      }
    ]
  },
  {
    path: "login",
    component: LoginComponent
  }
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
