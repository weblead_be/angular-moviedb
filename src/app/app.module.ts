import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';

import { ReadMoreDirective } from "./shared/directives/read-more-directive";
import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SearchComponent } from "./movies/search/search.component";
import { GenresComponent } from "./movies/genres/genres.component";
import { MovieListComponent } from "./movies/movie-list/movie-list.component";
import { MovieItemComponent } from "./movies/movie-list/movie-item/movie-item.component";
import { MovieDetailComponent } from "./movies/movie-detail/movie-detail.component";
import { SearchPipe } from "./movies/search/search.pipe";
import { MovieListResolver } from "./shared/resolvers/movie-list.resolver";
import { MovieDetailResolver } from "./shared/resolvers/movie-detail.resolver";
import { MovieFavoritesComponent } from "./movies/movie-favorites/movie-favorites.component";
import { MovieReviewsComponent } from "./movies/movie-detail/movie-reviews/movie-reviews.component";
import { MovieDetailReviewResolver } from "./shared/resolvers/movie-detail-review.resolver";
import { MovieReviewsItemComponent } from "./movies/movie-detail/movie-reviews/movie-reviews-item/movie-reviews-item.component";
import { AlertComponent } from './shared/alert/alert.component';
import { MovieRootComponent } from './movies/movie-root.component';
import { DashboardRootComponent } from './dashboard/dashboard-root.component';
import { LoginComponent } from './login/login.component';
import { NavbarAccountComponent } from './shared/navbar-account/navbar-account.component';
import { ProfileComponent } from './dashboard/profile/profile.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
   declarations: [
      AppComponent,
      SearchComponent,
      GenresComponent,
      MovieRootComponent,
      MovieListComponent,
      MovieItemComponent,
      MovieDetailComponent,
      SearchPipe,
      MovieFavoritesComponent,
      MovieReviewsComponent,
      MovieReviewsItemComponent,
      ReadMoreDirective,
      AlertComponent,
      DashboardRootComponent,
      LoginComponent,
      NavbarAccountComponent,
      ProfileComponent,
      RegisterComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      AppRoutingModule,
      FormsModule,
      ReactiveFormsModule,
      LazyLoadImageModule,
      AngularFireModule.initializeApp(environment.firebase,
      'angular-auth-firebase'),
      AngularFireDatabaseModule,
      AngularFireAuthModule,
      AngularFireStorageModule
   ],
   providers: [
      MovieListResolver,
      MovieDetailResolver,
      MovieDetailReviewResolver
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule {}
