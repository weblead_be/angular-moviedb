import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

import { GenresListRemoteObject, GenreListRemote } from './genre.remote.model';
import { MovieService } from '../movie.service';
import { Promise } from 'q';


@Injectable({
  providedIn: 'root'
})
export class GenresService {

  movieDbApiKey: string = this.movieService.movieDbApiKey;

  private genresListObs = new BehaviorSubject<any>(null);
  public genresListObs$ = this.genresListObs.asObservable();

  private genreId: number;

  constructor(private http: HttpClient, private movieService: MovieService) { }


  getGenresListRemote() {
    return this.http.get<GenresListRemoteObject[]>(
      'https://api.themoviedb.org/3/genre/movie/list?api_key='+this.movieDbApiKey, 
      { observe: 'body' })
      .pipe(
        map( response => { 
          return response['genres'];
        })
      );
    }

  /* PROMIS EXAMPLE */
  // getGenresListRemote() {
  //   return Promise((resolve, reject)  => {
  //     this.http.get('https://api.themoviedb.org/3/genre/movie/list?api_key='+this.movieDbApiKey)
  //     .toPromise()
  //     .then(result => resolve(result['genres']));
  //   })
  // }

  // here we set/change value of the observable
  updateGenreNameObs(data: GenreListRemote[], genre:number) { 
    this.genresListObs.next(data);
    this.genreId = genre;
  }

  // listens to observable with async api genreList 
  getGenreById(genreList:GenreListRemote[], genreId:number) {
    
    if(genreList !== null && genreId !== null) {
      let obj = genreList.find(o => o.id === genreId);
      if(obj === undefined) {
        return 'Most Popular';
      } 
      
      return obj.name;
    }

  }


}
