export interface GenreListRemote {
	id: number;
	name: string;
}

export interface GenresListRemoteObject {
	genres: GenreListRemote[];
}