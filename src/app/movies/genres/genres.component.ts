import { Component, OnInit } from '@angular/core';
import { GenresService } from './genres.service';
import { GenreListRemote } from './genre.remote.model';
import { MovieService } from '../movie.service';
import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.scss']
})
export class GenresComponent implements OnInit {
  genre: number;
  genresRemote: GenreListRemote[];
  
  constructor(private genresService: GenresService, private movieService: MovieService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    
    this.route.params
      .subscribe(
        (params: Params) => {
          
          this.genre = +params['id'];

          this.getGenreList(this.genre);

        }
      );

  }

  getGenreList(genre:number) {

    this.genresService.getGenresListRemote()
      .subscribe(
        (response) => {
          this.genresRemote = response;

          // set data in service which is to be shared
          this.genresService.updateGenreNameObs(this.genresRemote, genre);
          
        },
        (error) => console.log(error)
      );
      
  }

}
