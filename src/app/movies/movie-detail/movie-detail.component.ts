import { MovieListRemote } from './../movie-list/movie-list-remote.model';
import { MovieFavoritesService } from './../movie-favorites/movie-favorites.service';
import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie.service';
import { ActivatedRoute, Params } from '@angular/router';
import { MovieDetailRemote } from '../movie-detail/movie-detail-remote.model';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {

  movieRemote: MovieDetailRemote;
  id: number;
  movieFavorites: MovieListRemote[];

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private movieFavoritesService: MovieFavoritesService) { 

    // Prefetch api data with resolver
    route.data.subscribe(
			data => {
        this.movieRemote = data['movieRemote'];
      }
    );
    
  }

  ngOnInit() {
    
    //this.movieRemote['poster_path'] = '';

    this.movieFavorites = this.movieFavoritesService.movieFavorites;

    this.movieRemote.favorite = this.movieFavoritesService.checkIfFavorite(this.movieRemote);

    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          
          //this.movie = this.movieService.getMovie(this.id);

          //this.getMovie(this.id);
    
        }
      );
  
  }

  onUpdateFavorite(event: any, movie: MovieListRemote) {
    console.log(movie);
    if(movie.favorite === false) {

      this.movieFavoritesService.addMovieToFavorites(movie);

    } else if(movie.favorite === true) {

      this.movieFavoritesService.removeMovieFromFavorites(movie);

    }
    
    event.stopPropagation();
    event.preventDefault();

  }

  // getMovie(id:number) {
  //   this.movieService.getMovieDetailRemote(id)
  //     .subscribe(
  //       (response) => {
  //         this.movieRemote = response;
  //         console.log(this.movieRemote);
  //       },
  //       (error) => console.log(error)
  //     );
  // }

  



}
