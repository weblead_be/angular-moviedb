import { Component, OnInit, Input } from "@angular/core";

import { Reviews } from "../movie-reviews.model";
import { ReviewsService } from '../reviews.service';
import { AuthService } from 'src/app/shared/authentication/auth.service';

@Component({
  selector: "app-movie-reviews-item",
  templateUrl: "./movie-reviews-item.component.html",
  styleUrls: ["./movie-reviews-item.component.scss"]
})
export class MovieReviewsItemComponent implements OnInit {
  @Input() review: Reviews;
  isCollapsed: boolean = true;

  constructor( private reviewsService: ReviewsService, public authService: AuthService ) {}

  ngOnInit() {
    //console.log(this.review);
  }

  onRemoveReview(review: Reviews) {
    this.reviewsService.removeReview(review);
  }

}
