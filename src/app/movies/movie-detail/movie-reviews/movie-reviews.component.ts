import { MovieService } from './../../movie.service';
import { ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { Reviews } from "./movie-reviews.model";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReviewsService } from './reviews.service';

@Component({
  selector: "app-movie-reviews",
  templateUrl: "./movie-reviews.component.html",
  styleUrls: ["./movie-reviews.component.scss"]
})
export class MovieReviewsComponent implements OnInit {
  reviewsRemote: Reviews[];
  lsReviews: any = [];
  reviews: Reviews[] = [];
  reviewForm: FormGroup;
  movieId: number;

  constructor(
    private route: ActivatedRoute, 
    private formBuilder: FormBuilder,
    private reviewsService: ReviewsService
  ) {

    this.movieId =  this.route.snapshot.params['id'];

    // Prefetch api data with resolver
    route.data.subscribe(data => {
      this.reviewsRemote = data["reviews"];
    });

    this.reviewsService.getReviewsFromLocalstorage(this.movieId)
      .subscribe(response => {
        
        this.lsReviews = [];

        if(response instanceof Object ) {
          Object.keys(response).forEach(key => {
            let array = {
              ...response[key],
              key: key,
              movieid: this.movieId
            }
            this.lsReviews.push(array);
          });
        }
        
        this.reviews = this.reviewsRemote.concat(this.lsReviews); // merge 2 arrays

      });

  }

  ngOnInit() {

    this.reviewForm = this.formBuilder.group({
      author: ['', [
        Validators.required
      ]],
      content: ['', [
        Validators.required
      ]]
    })

  }

  reviewSubmitHandler() {
    const formValue: Reviews = this.reviewForm.value;

    this.reviewsService.addReview(formValue, this.reviews, this.movieId);

    this.reviewForm.reset();

  }

}
