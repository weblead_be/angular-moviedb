export interface Reviews {
    author: string;
    content: string;
    movieid: number;
    id: string;
    url: string;
    key: string;
}

export interface ReviewsObject {
    id: number;
    page: number;
    results: Reviews[];
    total_pages: number;
    total_results: number;
}