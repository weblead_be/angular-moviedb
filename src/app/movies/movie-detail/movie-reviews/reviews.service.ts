import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MovieService } from './../../movie.service';

import { ReviewsObject, Reviews } from './movie-reviews.model';
import { map } from 'rxjs/operators';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {

  movieDbApiKey: string = this.movieService.movieDbApiKey;

  constructor(
    private http: HttpClient,
    private movieService: MovieService,
    private _firebaseDb: AngularFireDatabase
  ) { }

  /* Reviews List */
	getReviewsList(movieId:number) {

		return this.http.get<ReviewsObject>(
			'https://api.themoviedb.org/3/movie/'+movieId+'/reviews?api_key='+this.movieDbApiKey, 
			{ observe: 'body' })
			.pipe(
				map( response => { 
          //console.log(response.results);
          let reviewsList = response.results.map(element=> {
            element['movieid'] = movieId;
            delete element.id;
            delete element.url;
          });
					return response.results;
				}
			)
    );
    
  }

  getReviewsFromLocalstorage(movieId:number) {

    return this._firebaseDb.object('reviews/'+movieId).valueChanges();

  }

  addReview(review: Reviews, reviewsList: Reviews[], movieId: number) {

    const itemRef = this._firebaseDb.list('reviews/'+movieId);
    itemRef.push({ author: review.author, content: review.content })

  }

  removeReview(review: Reviews) {

    const itemRef = this._firebaseDb.list('reviews/'+review.movieid);
    itemRef.remove(review.key);
    
  }
  
}
