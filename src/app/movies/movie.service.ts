import { Injectable } from '@angular/core';
import { SearchPipe } from './search/search.pipe';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { MovieListRemote, MovieListRemoteObject } from './movie-list/movie-list-remote.model';

import { MovieDetailRemote } from './movie-detail/movie-detail-remote.model';
import { MovieFavoritesService } from './movie-favorites/movie-favorites.service';


@Injectable({
  providedIn: 'root'
})

export class MovieService {

	public movieDbApiKey: string = '6cd457a8f1ab86282d6fc6f3acd1c9fe';
	searchInput: string;
	movieFavorites: MovieListRemote[] = this.movieFavoritesService.movieFavorites;

	constructor(private http: HttpClient, private movieFavoritesService: MovieFavoritesService) { }

	/* Movie detail */
	getMovieDetailRemote(movieId:number) {
		return this.http.get<MovieDetailRemote>(
			'https://api.themoviedb.org/3/movie/'+movieId+'?api_key='+this.movieDbApiKey, 
			{ observe: 'body' })
			.pipe(
				map( response => { 
					const poster_path = response['poster_path'];
					const backdrop_path = response['backdrop_path'];
					const release_date = response['release_date'];

					response['poster_path'] = 'https://image.tmdb.org/t/p/w500' + poster_path;

					if(backdrop_path) {
						response['backdrop_path'] = 'https://image.tmdb.org/t/p/w1280' + backdrop_path;
					}

					let release_year = release_date.split('-');
					response['release_year'] = release_year[0];
					
					response['favorite'] = false;
					return response;
				}
			)
		);
	}

	/* Movie List */
	getMoviesListRemote(genre:number, search:string, page:number) {
		const apiPrefix = 'https://api.themoviedb.org/3/';
		
		let apiUrl = apiPrefix + 'discover/movie?sort_by=popularity.desc&include_adult=false';

		if(genre) {
			apiUrl = apiPrefix + 'discover/movie?sort_by=popularity.desc&include_adult=false&with_genres=' + genre;
		}
		if(search) {
			apiUrl = apiPrefix + 'search/movie?query=' + search;
		}
		if(page) {
			const nextPage = page + 1;
			apiUrl += '&page=' + nextPage;
		}

		return this.http.get<MovieListRemoteObject>(
			apiUrl+'&api_key='+this.movieDbApiKey, 
			{ observe: 'body' })
			.pipe(
				map( response => { 

					let listItems = response;

					// remove items without a cover
					listItems.results = listItems.results.filter(element => element.poster_path !== null);
					
					listItems.results = listItems.results.map(element => {
						element.favorite = false;
						element.poster_path = 'https://image.tmdb.org/t/p/w500' + element.poster_path;

						return element;

					});

					return listItems;
				}
			)
		);
	}

}
