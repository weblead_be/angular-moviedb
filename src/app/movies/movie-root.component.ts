import { Component, OnInit } from '@angular/core';
import { MovieFavoritesService } from './movie-favorites/movie-favorites.service';
import { Router, Event, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { AuthService } from './../shared/authentication/auth.service';

@Component({
  selector: 'app-movie-root',
  templateUrl: './movie-root.component.html',
  styleUrls: ['./movie-root.component.scss']
})
export class MovieRootComponent implements OnInit {

  loading: boolean = false;
  favoritesTotal: number = 0;

  constructor( private movieFavoritesService: MovieFavoritesService, private router: Router, public authService: AuthService ) {

    /* for global loading when switching pages */
    router.events.subscribe((routerEvent: Event) => {
      this.checkRouterEvent(routerEvent);
    });

  }

  ngOnInit() {
    this.favoritesTotal = this.movieFavoritesService.getFavoritesTotal();

    this.movieFavoritesService.updateMovieFavorites.subscribe(data => { 
      this.favoritesTotal = data.length;
    });
  }

  checkRouterEvent(routerEvent: Event): void {
    if (routerEvent instanceof NavigationStart) {
      this.loading = true;
    }

    if (routerEvent instanceof NavigationEnd ||
      routerEvent instanceof NavigationCancel ||
      routerEvent instanceof NavigationError) {
      this.loading = false;
    }
  }

}
