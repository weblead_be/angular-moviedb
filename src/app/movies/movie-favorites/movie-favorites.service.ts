import { Injectable } from '@angular/core';
import { MovieListRemote } from '../movie-list/movie-list-remote.model';
import { Subject } from 'rxjs';

import { AlertService } from './../../shared/alert/alert.service';

@Injectable({
  providedIn: 'root'
})
export class MovieFavoritesService {

  movieFavorites: MovieListRemote[] = [];
  updateMovieFavorites: Subject<MovieListRemote[]> = new Subject<MovieListRemote[]>();

  constructor(private alertService: AlertService) { 

    const movieFavoritesLocalStorage = localStorage.getItem('movieFavorites'); 
    if( movieFavoritesLocalStorage !== null) {
      this.movieFavorites = JSON.parse(movieFavoritesLocalStorage);
    }

  }

  addMovieToFavorites(movie: MovieListRemote) {
    this.movieFavorites.push(movie);
    movie.favorite = true;

    this.doFavoriteUpdate(this.movieFavorites);

    this.alertService.success(movie.title + ' is added to favorites!');

  }

  removeMovieFromFavorites(movie: MovieListRemote) {
    this.movieFavorites = this.movieFavorites.filter(item => item.id !== movie.id);
    movie.favorite = false;

    this.doFavoriteUpdate(this.movieFavorites);

    this.alertService.success(movie.title + ' is removed from favorites!');

  }

  doFavoriteUpdate(newFavorites: MovieListRemote[]) {

    this.updateMovieFavorites.next(newFavorites);
    
    // write to localstorage
    console.log('writing to localstorage', newFavorites);
    localStorage.setItem('movieFavorites', JSON.stringify(newFavorites));

  }

  clearFavorites() {
    this.movieFavorites = [];
    this.doFavoriteUpdate(this.movieFavorites);
  }

  checkIfFavorite(movie: any) {
    const movieId = movie.id;

    //console.log('YYYYYY', this.movieFavorites);
    let check = this.movieFavorites.find(element => element.id == movie.id);

    if(check !== undefined) {
      return true;
    } else {
      return false;
    }
  }

  getFavoritesTotal() {
    return this.movieFavorites.length;
  }

}
