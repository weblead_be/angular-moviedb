import { MovieFavoritesService } from './movie-favorites.service';
import { Component, OnInit } from '@angular/core';

import { MovieListRemote } from '../movie-list/movie-list-remote.model';


@Component({
  selector: 'app-movie-favorites',
  templateUrl: './movie-favorites.component.html',
  styleUrls: ['./movie-favorites.component.scss']
})
export class MovieFavoritesComponent implements OnInit {

  movieFavorites: MovieListRemote[];

  constructor( private movieFavoritesService: MovieFavoritesService ) { }

  ngOnInit() {

    
    
    this.movieFavorites = this.movieFavoritesService.movieFavorites;

    //console.log(this.movieFavorites);

    this.movieFavoritesService.updateMovieFavorites.subscribe(data => { 
      console.log('obs - comp', data);
      this.movieFavorites = data;
    });


  }

  onClearFavorites() {
    this.movieFavoritesService.clearFavorites();
  }

}
