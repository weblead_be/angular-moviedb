import { Component, OnInit, Input } from '@angular/core';
import { MovieListRemote } from '../movie-list-remote.model';
import { MovieFavoritesService } from './../../movie-favorites/movie-favorites.service';

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.scss']
})
export class MovieItemComponent implements OnInit {

  @Input() movie: MovieListRemote;
  @Input() movieIndex: number;
  @Input() favoritesList: boolean;

  favorite: any = false;
  movieFavorites: MovieListRemote[];

  constructor( private movieFavoritesService: MovieFavoritesService ) { }

  ngOnInit() {

    this.movie.favorite = this.movieFavoritesService.checkIfFavorite(this.movie);

  }

  onUpdateFavorite(event: any, movie: MovieListRemote) {
    
    if(movie.favorite === false) {

      this.movieFavoritesService.addMovieToFavorites(movie);

    } else if(movie.favorite === true) {

      this.movieFavoritesService.removeMovieFromFavorites(movie);

    }
    
    event.stopPropagation();
    event.preventDefault();

  }



}
