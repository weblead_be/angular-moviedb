import { Component, OnInit, HostListener } from '@angular/core';
import { MovieService } from '../movie.service';
import { ActivatedRoute, Params } from '@angular/router';
import { MovieListRemote, MovieListRemoteObject } from './movie-list-remote.model';
import { GenresService } from '../genres/genres.service';
import { GenreListRemote } from '../genres/genre.remote.model';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {

  moviesRemote: MovieListRemote[];
  genre: number;
  genreList: GenreListRemote[];
  genreName: string;
  search: string;
  moviesCurrentPage: number;
  moviesTotalPages: number;
  loading: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private movieService: MovieService,
    private genreService: GenresService
  ) {

    // Prefetch api data (moviesRemote) with resolver
    route.data.subscribe(
      data => { 
        const moviesRemoteObject = data['moviesRemote'];
        this.moviesCurrentPage = moviesRemoteObject.page;
        this.moviesTotalPages = moviesRemoteObject.total_pages;
        this.moviesRemote = moviesRemoteObject.results; 
      }
		);

  }

  ngOnInit() {

    //this.moviesCurrentPage = this.moviesRemote

    this.route.params
      .subscribe(
        (params: Params) => {
          
          this.genre = +params['id'];
          this.search = params['query'];

          this.getGenreNameById(this.genre);

        }
      );
  }

  getMoviesList() {
    if(this.loading === false) {
      this.loading = true;

      this.movieService.getMoviesListRemote(this.genre, this.search, this.moviesCurrentPage)
        .subscribe(
          (response) => {
            const newMoviesRemote = this.moviesRemote.concat(response.results);
            this.moviesRemote = newMoviesRemote;
            this.moviesCurrentPage = response.page;    
            
            this.loading = false;
  
          },
          (error) => console.log(error)
        );
    }
    
  }

  getGenreNameById(genre: number) {
    this.genreService.genresListObs$.subscribe(data => { 
      
      this.genreList = data;
      this.genreName = this.genreService.getGenreById(this.genreList, genre);

    });
  }

  @HostListener('window:scroll', ['$event'])
    onScroll($event: Event): void {
      
    const offsetHeight  = document.body.offsetHeight - 300;
    if((window.innerHeight + window.scrollY) >= offsetHeight) {

      this.moviesTotalPages !== this.moviesCurrentPage ? this.getMoviesList() : console.log('end reached mofo')

    } 

  }
 


}
