import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Input() 'searchInput':string;

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

  }

  onSearch() {

    this.router.navigateByUrl('/search/'+this.searchInput);

  }

}
