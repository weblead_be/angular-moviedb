// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDZ-vxYLbNcoHLtINstmdaWx1GHdVDEMSA",
    authDomain: "youflix-ced56.firebaseapp.com",
    databaseURL: "https://youflix-ced56.firebaseio.com",
    projectId: "youflix-ced56",
    storageBucket: "youflix-ced56.appspot.com",
    messagingSenderId: "69766349089"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
